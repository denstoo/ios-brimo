
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String



def static "common.choiceSumberDana.choiceSumberDanaWithName"(
    	String text	) {
    (new common.choiceSumberDana()).choiceSumberDanaWithName(
        	text)
}


def static "common.screenshot.takeScreenshotAsCheckpoint"() {
    (new common.screenshot()).takeScreenshotAsCheckpoint()
}

/**
	 * Open and return a connection to database
	 * @param dataFile absolute file path
	 * @return an instance of java.sql.Connection
	 */
def static "database.methods.connectDB"(
    	String url	
     , 	String dbname	
     , 	String port	
     , 	String username	
     , 	String password	) {
    (new database.methods()).connectDB(
        	url
         , 	dbname
         , 	port
         , 	username
         , 	password)
}

/**
	 * execute a SQL query on database
	 * @param queryString SQL query string
	 * @return a reference to returned data collection, an instance of java.sql.ResultSet
	 */
def static "database.methods.executeQuery"(
    	String queryString	) {
    (new database.methods()).executeQuery(
        	queryString)
}


def static "database.methods.executeUpdate"(
    	String queryString	) {
    (new database.methods()).executeUpdate(
        	queryString)
}


def static "database.methods.updateRefNum"() {
    (new database.methods()).updateRefNum()
}


def static "database.methods.closeDatabaseConnection"() {
    (new database.methods()).closeDatabaseConnection()
}

/**
	 * Execute non-query (usually INSERT/UPDATE/DELETE/COUNT/SUM...) on database
	 * @param queryString a SQL statement
	 * @return single value result of SQL statement
	 */
def static "database.methods.execute"(
    	String queryString	) {
    (new database.methods()).execute(
        	queryString)
}

/**
	 * Check if element present in timeout
	 * @param to Katalon test object
	 * @param timeout time to wait for element to show up
	 * @return true if element present, otherwise false
	 */
def static "screenshot.capture.Screenshot"() {
    (new screenshot.capture()).Screenshot()
}


def static "common.connection.offlineMode"() {
    (new common.connection()).offlineMode()
}


def static "common.connection.onlineMode"() {
    (new common.connection()).onlineMode()
}


def static "operation.features.clickNumber"(
    	String number	) {
    (new operation.features()).clickNumber(
        	number)
}


def static "operation.features.clickAmountPulsa"(
    	String amount	) {
    (new operation.features()).clickAmountPulsa(
        	amount)
}
