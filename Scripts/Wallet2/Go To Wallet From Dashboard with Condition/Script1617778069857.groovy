import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.detroitlabs.katalonmobileutil.touch.Swipe as Swipe
import com.detroitlabs.katalonmobileutil.touch.Swipe.SwipeDirection as SwipeDirection

WebUI.callTestCase(findTestCase('General/Database Connect'), [:], FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'database.methods.executeUpdate'(('DELETE FROM tbl_history_purchase_ewallet WHERE username = "' + username.toString()) +
	'"')

CustomKeywords.'database.methods.executeUpdate'(('DELETE FROM tbl_list_purchase WHERE username = "' + username.toString()) +
	'" and group_id = "4"')

if (condition.toString() == 'NEW') {
	CustomKeywords.'database.methods.executeUpdate'(('DELETE FROM tbl_history_purchase_ewallet WHERE username = "' + username.toString()) +
		'"')

	CustomKeywords.'database.methods.executeUpdate'(('DELETE FROM tbl_list_purchase WHERE username = "' + username.toString()) +
		'" and group_id = "4"')
} else if(condition.toString() == 'history2') {
	
	CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_history_purchase_ewallet (username, value) VALUES ('"+ username.toString() +"','"+ GlobalVariable.dbHistory2 +"')"))
	
	CustomKeywords.'database.methods.executeUpdate'(('DELETE FROM tbl_list_purchase WHERE username = "' + username.toString()) + '" and group_id = "4"')
	
} else if(condition.toString() == 'history10') {
	
	CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_history_purchase_ewallet (username, value) VALUES ('"+ username.toString() +"','"+ GlobalVariable.dbHistory10 +"')"))
	
	CustomKeywords.'database.methods.executeUpdate'(('DELETE FROM tbl_list_purchase WHERE username = "' + username.toString()) + '" and group_id = "4"')
	
} else if(condition.toString() == 'bookmark1') {
	
	CustomKeywords.'database.methods.executeUpdate'(('DELETE FROM tbl_history_purchase_ewallet WHERE username = "' + username.toString()) + '"')
	
	CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_list_purchase (username,purchase_number,purchase_nickname,status,purchase_type_id,group_id,F1) VALUES ('"+ username.toString() +"','082210112982','iqbal55',1,15,4,'301341')"))
	
} else if(condition.toString() == 'bookmark20' || condition.toString() == 'search') {
	
	for(int i=1;i<=20;i++) {
		
		CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_list_purchase (username,purchase_number,purchase_nickname,status,purchase_type_id,group_id,F1) VALUES ('"+ username.toString() +"','082210112982','@iqbal"+ i +"',1,15,4,'301341')"))
	}
	
} else {
	
	CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_history_purchase_ewallet (username, value) VALUES ('"+ username.toString() +"','"+ GlobalVariable.dbHistory2 +"')"))
	
	CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_list_purchase (username,purchase_number,purchase_nickname,status,purchase_type_id,group_id,F1) VALUES ('"+ username.toString() +"','081288626368','iqbal55',1,15,4,'301342')"))
}

device_Height = Mobile.getDeviceHeight()
device_Width = Mobile.getDeviceWidth()
int startX = device_Width / 2
int endX = startX
int startY = device_Height * 0.30
int endY = device_Height * 0.70

Mobile.swipe(startX, startY, endX, endY)

Mobile.delay(3)

if(Mobile.verifyElementExist(findTestObject('/General/XCUIElementTypeButton - BrimoEyeOpen'), 10, FailureHandling.OPTIONAL) == true) {
	Mobile.tap(findTestObject('/General/XCUIElementTypeButton - BrimoEyeOpen'), 0)
} else {
	Mobile.tap(findTestObject('/General/XCUIElementTypeButton - BrimoEyeClose'), 0)
}

Mobile.tap(findTestObject('Feature Dashboard Home/XCUIElementTypeStaticText - Lainnya'), 0)

Mobile.verifyElementExist(findTestObject('Feature Dashboard Home/XCUIElementTypeStaticText - Dompet Digital'), 0)

CustomKeywords.'screenshot.capture.Screenshot'()

Mobile.tap(findTestObject('Feature Dashboard Home/XCUIElementTypeStaticText - Dompet Digital'), 0)

if (Mobile.verifyElementNotExist(findTestObject('Wallet Form/XCUIElementTypeOther - Dompet Digital'), 5, FailureHandling.OPTIONAL) ==
true) {
	//    Mobile.tap(findTestObject('Feature Dashboard Home/XCUIElementTypeButton - Fitur Lainnya'), 0)

	//ini swipe start
//	device_Height = Mobile.getDeviceHeight()
//	device_Width = Mobile.getDeviceWidth()
//	int startX = device_Width / 2
//	int endX = startX
//	int startY = device_Height * 0.30
//	int endY = device_Height * 0.70
	
	//Mobile.swipe(startX, startY, endX, endY)
	
	//end swipe
	
	Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)

	//	Scroll.scrollListToElementWithText('Donasi', 0)
	Mobile.verifyElementExist(findTestObject('Feature Dashboard Home/XCUIElementTypeStaticText - Dompet Digital'), 0)

	Mobile.tap(findTestObject('Feature Dashboard Home/XCUIElementTypeStaticText - Dompet Digital'), 0)
}

Mobile.verifyElementExist(findTestObject('Feature Dashboard Home/XCUIElementTypeStaticText - Dompet Digital'), 0)

Mobile.delay(3)

CustomKeywords.'screenshot.capture.Screenshot'()

WebUI.callTestCase(findTestCase('General/Database Close'), [:], FailureHandling.STOP_ON_FAILURE)
