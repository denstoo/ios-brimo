Feature: Login

	@Valid
  Scenario Outline: User login for first time
    Given I start application for first time
    When I already have an account
    Given I start application
    When I want login
    When I try first login with existing account <username> and <password> but have not pin
    When I verification deep link from account <username>
    And I create pin with <pin>
    And I confirm pin with <pin>
    Then I successfully go to dashboard for first time

    Examples: 
      | username   | password   | pin		 |
      | taufan123456 | Jakarta123 | 123457 |
      
   @Valid
   Scenario Outline: User login existing in another device
    Given I start application
    When I want login
    When I try login another device with existing account <username> and <password>
    When I confirm pin with <pin>
    When I verification deep link from account <username>
    Then I successfully go to dashboard for first time

    Examples: 
      | username   | password   | pin		 |
      | taufan123456 | Jakarta123 | 123457 |
      
   @Valid
   Scenario Outline: User login with existing account
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard

    Examples: 
      | username   | password   |
      | taufan123456 | Jakarta123 |
      
   @401
   Scenario Outline: User login with existing account
   Given I start application
   When I want login
   When I try login with existing account <username> and <password>
   Then I successfully go to dashboard
    Examples: 
      | username   | password   |
      | taufan123456 | Jakarta123 |
      
 @After
   Scenario: User login with existing account
   Given I start application
   Given ganti wifi
   When I want login
   
   @Invalid
   Scenario Outline:
   When I try login with existing account <username> and <password>
   Then <message> message appears
    Examples: 
      | username			| password	| message													|
      | asal					| asal			| Silakan periksa kembali username dan password Anda |
      | taufan123456	| asal321		| Username / password tidak valid |

      
      
      
      
      
      
      
      
      
      
      
      
      