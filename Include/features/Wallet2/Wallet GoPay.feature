#Author: iqbal@id.sharingvision.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

Feature: Wallet GoPay
  I want to top up GoPay wallet and check the detail in Dompet Digital Page

  @tag1
  Scenario Outline: User go to Wallet Page then check the history and saved wallet (tc 101-106)
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    When I saw my top up wallet history

    Examples: 
      | username   | password   | condition		|
      | brimosv010 | Jakarta123 |	NEW					|
      | brimosv010 | Jakarta123 |	history2		|
      | brimosv010 | Jakarta123 |	history10		|
      | brimosv010 | Jakarta123 |	bookmark1		|
      | brimosv010 | Jakarta123 |	bookmark20	|
      
      
      #Condition 
      #NEW : delete db history dan bookmark
      #history2 : insert 2 history ada di GlobalVariable
      #history10 : insert 10 history ada di GlobalVariable
      #bookmark1 : insert 1 wallet ada di GlobalVariable
      #bookmark20 : insert 20 wallet ada di GlobalVariable
      #once : insert 2 history dan 1 bookmark ada di GlobalVariable
      
	@tag2
  Scenario Outline: User go to Wallet Page then check the search for saved wallet list (tc 107-114, 118, 119)
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    When I saw my top up wallet history
    And I want to verify search for wallet list when internet is <connection>

    Examples: 
      | username   | password   | condition		| connection 	|
      #| brimosv010 | Jakarta123 |	search			| on					|
      #| brimosv010 | Jakarta123 |	search			| off					|
      #| brimosv010 | Jakarta123 |	search			| out session	|
      
      #condition
      #search : insert 20 wallet ada di GlobalVariable
      
      #connection 
      #on : mode pesawat "on"
      #off : mode pesawat "off"
      #out session : ada delay 420 detik agar session habis
      
  @tag3
  Scenario Outline: User go to Wallet Page then use Top Up Baru (tc 115)
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    When I saw my top up wallet history
    When I want to add recipient of wallet
    When I insist adding recipient of wallet in condition with <wallet>, <type> and <walletNumber>

    Examples: 
      | username   | password   | condition		| wallet	|	type			|	walletNumber	|
      | brimosv010 | Jakarta123 |	NEW					| Gopay		|	Customer	|	082210112982	|
      | brimosv010 | Jakarta123 |	NEW					| Gopay		|	Driver		|	081288626368	|
      
      #Wallet : Gopay, Shopeepay, DANA, OVO, LinkAja
      
  @tag4
  Scenario Outline: User go to Wallet Page then use last history (tc 116)
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    When I saw my top up wallet history
    When I top up wallet from my history

    Examples: 
      | username   | password   | condition		|
      | brimosv010 | Jakarta123 |	once				|
      
  @tag5
  Scenario Outline: User go to Wallet Page then use Wallet List (tc 117)
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    When I saw my top up wallet history
   	When I top up wallet from my list

    Examples: 
      | username   | password   | condition		|
      | brimosv010 | Jakarta123 |	once				|    

      
      
  @tag6
  Scenario Outline: User go to Top Up Baru Page then Input New GoPay Recipient by Input (tc 201-208)
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    #When I saw my top up wallet history
    When I want to add recipient of wallet
    When I adding recipient of wallet with <wallet> and <type>
    And I add <walletNumber> of recipient by input
    And I go to nominal page with <condition> condition

    Examples: 
      | username   | password   | condition		| wallet	|	type			|	walletNumber	| condition	|
      | brimosv010 | Jakarta123 |	NEW					| Gopay		|	Customer	|	082210112982	| normal		|
      | brimosv010 | Jakarta123 |	NEW					| Gopay		|	Customer	|	123456578910	| normal		|
      | brimosv010 | Jakarta123 |	NEW					| Gopay		|	Driver		|	081288626368	| normal		|
      | brimosv010 | Jakarta123 |	NEW					| Gopay		|	Driver		|	123456578910	| normal		|
      
  @tag7
  Scenario Outline: User go to Top Up Baru Page then Input New GoPay Recipient by Contact (tc 209-210)
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    #When I saw my top up wallet history
    When I want to add recipient of wallet
    When I adding recipient of wallet with <wallet> and <type>
    And I add <contactName> of recipient by contact
    And I go to nominal page with <condition> condition

    Examples: 
      | username   | password   | condition		| wallet	|	type			|	contactName			| condition	|
      | brimosv010 | Jakarta123 |	NEW					| Gopay		|	Customer	|	Gopay1					| normal		|
      | brimosv010 | Jakarta123 |	NEW					| Gopay		|	Customer	|	gopay not valid	| normal		|
      | brimosv010 | Jakarta123 |	NEW					| Gopay		|	Driver		|	Gopay1					| normal		|
      | brimosv010 | Jakarta123 |	NEW					| Gopay		|	Driver		|	gopay not valid	| normal		|
      
  @tag8
  Scenario Outline: User go to Top Up Baru Page then Input New GoPay Recipient by Input (tc 211-212)
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    #When I saw my top up wallet history
    When I want to add recipient of wallet
    When I adding recipient of wallet with <wallet> and <type>
    And I add <walletNumber> of recipient by input
    And I go to nominal page with <page_condition> condition

    Examples: 
      | username   | password   | condition		| wallet	|	type			|	walletNumber	| page_condition 		|
      #| brimosv010 | Jakarta123 |	NEW					| Gopay		|	Customer	|	082210112982	| normal						|
      #| brimosv010 | Jakarta123 |	NEW					| Gopay		|	Customer	|	082210112982	| out session		|
      | brimosv010 | Jakarta123 |	NEW					| Gopay		|	Customer	|	082210112982	| no internet		|
      
      
  @tag9
  Scenario Outline: User go to nominal page by new number and verify the check box simpan sebagai is enable (tc 301)
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    #When I saw my top up wallet history
    When I want to add recipient of wallet
    When I adding recipient of wallet with <wallet> and <type>
    And I add <walletNumber> of recipient by input
    And I go to nominal page with <condition> condition
    Then I success go to nominal page and see status checkbox simpan sebagai

    Examples: 
      | username   | password   | condition		| wallet	|	type			|	walletNumber	| condition 		|
      | brimosv010 | Jakarta123 |	NEW					| Gopay		|	Customer	|	082210112982	| normal				|
      
  @tag10
  Scenario Outline: User go to nominal page by history and verify the check box simpan sebagai is enable (tc 302)
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    When I saw my top up wallet history
    When I top up wallet from my history
    Then I success go to nominal page and see status checkbox simpan sebagai

    Examples: 
      | username   | password   | condition			|
      | brimosv010 | Jakarta123 |	history2			|
      
   @tag11
  Scenario Outline: User go to nominal page by bookmark and verify the check box simpan sebagai is disable (tc 303 & 304)
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    When I saw my top up wallet history
   	When I top up wallet from my list
   	Then I success go to nominal page and see status checkbox simpan sebagai

    Examples: 
      | username   | password   | condition		|
      | brimosv010 | Jakarta123 |	bookmark1		|
      | brimosv010 | Jakarta123 |	once				|
      
      
  @tag12
  Scenario Outline: User go to nominal page and tap check box simpan selanjutnya then verify input name is enable(tc 305 & 306)
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    When I saw my top up wallet history
    When I top up wallet from my history
    Then I success go to nominal page and see status checkbox simpan sebagai
    When I verify field input nama is enable after tap checkbox simpan

    Examples: 
      | username   | password   | condition			|
      | brimosv010 | Jakarta123 |	history2			|
      
      
  @tag13
  Scenario Outline: User go to nominal page and input valid amount for wallet (tc 307)
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    When I saw my top up wallet history
   	When I top up wallet from my list
   	Then I success go to nominal page and see status checkbox simpan sebagai
   	When I input valid <walletAmount> for wallet

    Examples: 
      | username   | password   | condition		| walletAmount	|
      | brimosv010 | Jakarta123 |	bookmark1		|	10000					|
      
  @tag14
  Scenario Outline: User go to nominal page and input invalid amount for wallet in certain condition (tc 308, 309 & 310)
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    When I saw my top up wallet history
   	When I top up wallet from my list with <funds> fund for <username>
   	Then I success go to nominal page and see status checkbox simpan sebagai
   	When I input invalid amount for wallet in <terms>

    Examples: 
      | username   | password   | condition		| terms						| funds 	|
      | brimosv010 | Jakarta123 |	bookmark1		|	over balance		| 1				|
      | brimosv010 | Jakarta123 |	bookmark1		|	limit wallet		| 5				|
      | brimosv010 | Jakarta123 |	bookmark1		|	peculiar number	| 5				|
   
  @tag15
  Scenario Outline: User go to nominal page and check the list of accounts of fund (tc 313,314,315)
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    When I saw my top up wallet history
   	When I top up wallet from my list with <funds> fund for <username>
   	Then I success go to nominal page and see status checkbox simpan sebagai
   	When I input valid <walletAmount> for wallet
   	And I want to verify the account list

    Examples: 
      | username   | password   | condition		| walletAmount	| funds |
      | brimosv010 | Jakarta123 |	bookmark1		|	10000					| 1		  |
      | brimosv010 | Jakarta123 |	bookmark1		|	10000					| 5		  |
      
      
  @tag15a
  Scenario Outline: User go to nominal page and check the list of accounts for freeze, dormant and closed account (tc 316)
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    When I saw my top up wallet history
   	When I top up wallet from my list with <funds> fund for <username>
   	Then I success go to nominal page and see status checkbox simpan sebagai
   	When I input valid amount for wallet and choose <accounts> account

    Examples: 
      | username   | password   | condition		| funds | accounts		|
      | brimosv010 | Jakarta123 |	bookmark1		| 4		  | freeze			|
      | brimosv010 | Jakarta123 |	bookmark1		| 4		  | dormant			|
      | brimosv010 | Jakarta123 |	bookmark1		| 4		  | closed			|
      
      
  @tag16
  Scenario Outline: User go to nominal page and check expired and no connection condition (tc 317 - 318)
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    When I saw my top up wallet history
   	When I top up wallet from my list
   	Then I success go to nominal page and see status checkbox simpan sebagai
   	When I input valid <walletAmount> for wallet
   	And I verify nominal page when <page_condition> condition

    Examples: 
      | username   | password   | condition		| walletAmount	| page_condition 	|
      | brimosv010 | Jakarta123 |	bookmark1		|	10000					|		no internet		|
      #| brimosv010 | Jakarta123 |	bookmark1		|	10000					|		out sessiion	|		
  
  @tag17
  Scenario Outline: User go to confirmation page and verify the fee and saving or giro account (tc 401,402,403)
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    When I saw my top up wallet history
   	When I top up wallet from my list with <funds> fund for <username>
   	Then I success go to nominal page and see status checkbox simpan sebagai
   	When I input valid <walletAmount> for wallet
   	And I tap top up button
   	Then I successfully go to confirmation page
   	When I check the fee for top up wallet
   	And I tap top up button
   	Then I successfully in input PIN page

    Examples: 
      | username   | password   | condition		| walletAmount	| funds 		|
      | brimosv010 | Jakarta123 |	bookmark1		|	10000					| saving		|
      | brimosv010 | Jakarta123 |	bookmark1		|	10000					| giro		  |
      
      
  @tag18
  Scenario Outline: User go to Input valid PIN after confirm Top Up Wallet (407)
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    When I saw my top up wallet history
   	When I top up wallet from my list
   	Then I success go to nominal page and see status checkbox simpan sebagai
   	When I input valid <walletAmount> for wallet
   	And I tap top up button
   	Then I successfully go to confirmation page
   	And I tap top up button
   	Then I successfully in input PIN page
   	When I input <PIN> for <status> condition

    Examples: 
      | username   | password   | condition		| walletAmount	| PIN 			| status	|
      | brimosv010 | Jakarta123 |	bookmark1		|	10000					|	112233		|	valid		|
      
  @tag19
  Scenario Outline: User go to Input invalid PIN in certain condition after confirm Top Up Wallet (tc 404, 405, 406)
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    When I saw my top up wallet history
   	When I top up wallet from my list
   	Then I success go to nominal page and see status checkbox simpan sebagai
   	When I input valid <walletAmount> for wallet
   	And I tap top up button
   	Then I successfully go to confirmation page
   	And I tap top up button
   	Then I successfully in input PIN page
   	When I input <PIN> for <status> condition

    Examples: 
      | username   | password   | condition		| walletAmount	| PIN 			| status					|
      | brimosv010 | Jakarta123 |	bookmark1		|	10000					|	111222		|	invalid					|
      | brimosv010 | Jakarta123 |	bookmark1		|	10000					|	112233		|	valid after try	|
      | brimosv010 | Jakarta123 |	bookmark1		|	10000					|	111222		|	invalid 3x			|
      
      
  @tag20
  Scenario Outline: User go to Input valid PIN  (408, 409, 410)
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    When I saw my top up wallet history
   	When I top up wallet from my list
   	Then I success go to nominal page and see status checkbox simpan sebagai
   	When I input valid <walletAmount> for wallet
   	And I tap top up button
   	Then I successfully go to confirmation page
   	And I tap top up button
   	Then I successfully in input PIN page
   	When I set up the <page_condition> condition for this page and input valid <PIN>

    Examples: 
      | username   | password   | condition		| walletAmount	| PIN 			| page_condition	|
      #| brimosv010 | Jakarta123 |	bookmark1		|	10000					|	112233	|	expired					|
      #| brimosv010 | Jakarta123 |	bookmark1		|	10000					|	112233	|	out session			|
      | brimosv010 | Jakarta123 |	bookmark1		|	10000					|	112233	|	no internet			|	
      
  @tag21
  Scenario Outline: User go to confirmation page and verify trx with suspend account (tc 506)
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    When I saw my top up wallet history
   	When I top up wallet from my list with <funds> fund for <username>
   	Then I success go to nominal page and see status checkbox simpan sebagai
   	When I input valid <walletAmount> for wallet
   	And I tap top up button
   	Then I successfully go to confirmation page
   	When I check the fee for top up wallet
   	And I tap top up button
   	Then I successfully in input PIN page
   	When I input <PIN> for <status> condition
   	Then I unsuccessful go to bukti transaksi page

    Examples: 
      | username   | password   | condition		| walletAmount	| funds 		| PIN 			| status	|
      | brimosv010 | Jakarta123 |	bookmark1		|	10000					| suspend		| 112233		|	valid		|
      
  @tag22
  Scenario Outline: User go to bukti transaksi page and verify condition (501, 503, 504, 505)
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to top up my wallet with <condition> for <username>
    When I saw my top up wallet history
   	When I top up wallet from my list
   	Then I success go to nominal page and see status checkbox simpan sebagai
   	When I input valid <walletAmount> for wallet
   	And I tap top up button
   	Then I successfully go to confirmation page
   	When I check the fee for top up wallet
   	And I tap top up button
   	Then I successfully in input PIN page
   	When I input <PIN> for <status> condition
   	Then I successful go to bukti transaksi page
   	When I verify bukti transaksi page in <page_condition> condition

    Examples: 
      | username   | password   | condition		| walletAmount	| PIN 			| status	| page_condition 	|
      | brimosv010 | Jakarta123 |	bookmark1		|	10000					|	112233		|	valid		| normal					|
  